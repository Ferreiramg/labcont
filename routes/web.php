<?php

$router->get('/', function () {
    echo file_get_contents(dirname(__DIR__) . '/public/index.html');
});
$router->group(
    ['middleware' => 'auth'],
    function () use ($router) {
        $router->get('user', 'User@index');
        $router->post('avatar', 'User@upload');
        $router->post('user', 'User@update');
        $router->get('myconfigs', 'User@getConfig');
        $router->post('configs', 'User@setConfig');

        $router->get('operacoes', 'Empresas@listOperacoes');
        $router->post('empresa', 'Empresas@store');
        $router->post('empresa/update', 'Empresas@update');
        $router->get('empresas', 'Empresas@listByUser');

        $router->post('conta/delete', 'Conta@delete');
        $router->get('contas', 'Conta@index');
        $router->put('conta', 'Conta@update');
        $router->post('conta', 'Conta@store');
        $router->get('conta/next_id', 'Conta@nextId');
        $router->get('conta/analiticas', 'Conta@analiticas');

        $router->post('lancamento', 'Lancamentos@store');
        $router->post('lancamento/delete', 'Lancamentos@delete');
        $router->get('lancamentos', 'Lancamentos@listByUser');
        $router->get('lancamentos/diario', 'Lancamentos@diaryReport');

        $router->get('historicos', 'Historicos@index');
        $router->post('historicos', 'Historicos@store');
        $router->post('historico/delete', 'Historicos@delete');
        $router->post('historico/update', 'Historicos@update');

        $router->get('graficos/soma/sinteticas', 'Lancamentos@somaSinteticas');
        $router->get('relatorios/balancete/{start}/{end}/{id}', 'Relatorios@balancete');
        $router->post('relatorios/download/excel', 'Relatorios@downloadExcell');
        //$router->post('relatorios/download/excel', ['middleware' => 'download', 'uses' => 'Relatorios@downloadExcell']);
    }
);
//operacao de 'admin' ainda vou fazer o guard
//$router->post('operacoes', 'Empresas@insertOperacao');

//guests
$router->post('registro', 'Auth@register');
$router->post('sessions', 'Auth@authenticate');
$router->get('refresh', 'Auth@refresh');

$router->get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});
