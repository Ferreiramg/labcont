FROM php:7.4-fpm

#Install xdebug
RUN pecl install xdebug-3.0.0 && docker-php-ext-enable xdebug

CMD ["php-fpm"]