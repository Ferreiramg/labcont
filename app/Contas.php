<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contas extends Model
{
    protected $primaryKey = 'id';
    protected  $guarded = [];
    protected $fillable = [
        'id',
        'nome',
        'natureza',
        'redutor',
        'descricao',
        'classificacao',
        'tipo',
        'cstatus',
        'parent_id',
        'empresa_id',
        'user_id',
        'updated_at',
        'created_at'
    ];
    public function contas()
    {
        return $this->hasMany(self::class);
    }

    public function childrenContas()
    {
        return $this->hasMany(self::class)->with('contas');
    }

    public static function lastRecordId(int $id)
    {
        return self::where('user_id',$id)->max('redutor');
    }

    public static function analiticas(int $id): iterable
    {
        yield self::where('tipo', 1)->where('cstatus', true)->where('user_id',$id)->get();
    }

    public static function insertDemonstrativos(int $did, int $cid): bool
    {
        return DB::insert(
            "insert into demonstrativos (demonstrativo_id,conta_id) values(?,?)",
            [$did, $cid]
        );
    }

    public static function uniqueByUser(int $id, string $conta): bool
    {
        return self::where('user_id', $id)->where('classificacao', $conta)->count() === 0;
    }

    public static function getAllByUser(int $id): iterable
    {
        $result = self::where('user_id', $id)->orderBy('classificacao', 'asc')->get()->toArray();

        foreach ($result as $key => $contas) {

            $result[$key]['cgroup'] =
                DB::table('demonstrativos')
                ->select(['demonstrativo_id'])
                ->where('conta_id', '=', $contas['id'])->get();
        }

        yield $result;
        //   yield self::leftJoin('demonstrativos', 'demonstrativos.conta_id', '=', 'contas.id')
        //     ->where('user_id', $id)
        //    ->get();
    }
}

/**
 * 
 *  $categories = Contas::whereNull('parent_id')
 *      ->with('childrenContas')
 *     ->get();
 * 
 * 
 */
