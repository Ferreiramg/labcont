<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lancamento extends Model
{
    protected  $guarded = [];
    protected $primaryKey  = 'uuid';

    public static function getAllFromUser(int $id): array
    {
        return DB::select("select lancamentos.id,lote,valor,total,uuid, conta_tipo,movimento,
                lancamentos.created_at,fantasia,e.cnpj,e.id as eid,
                c.nome, c.classificacao as conta,c.tipo,
                (select descricao from historicos as h where h.id = lancamentos.historico_id) as historico 
                from lancamentos 
                inner join empresas as e ON e.id = empresa_id
                left join contas as c ON c.classificacao = conta_id  and c.cstatus=true
                where lancamentos.user_id ={$id}
                order by lancamentos.created_at desc;
        ");
    }

    public static function sumContasSinteticas(int $id,int $eid)
    {
        yield DB::select(
            "Select
            SubString(DP.conta_id from 1 for 1) as FNivel,
            sum(DP.total) as total, CC.nome
            from lancamentos DP
            join contas CC on CC.classificacao = SubString(DP.conta_id from 1 for 1)
            and CC.empresa_id={$eid}
            where DP.user_id={$id}
            group by  FNivel,CC.nome"
        );
    }
    public function getSumContas()
    {
        return DB::select(
            <<<SQL
                select 
                id,nome,classificacao,(select sum(valor) from lancamentos where empresa_id=2 and cc.id=credito_id order by 1) as credito,
                (select sum(valor) from lancamentos where empresa_id=2 and cc.id=debito_id order by 1) as debito
                from contas cc
SQL
        );
    }
}
