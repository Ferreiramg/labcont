<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'endereco', 'cep', 'cidade', 'phone', 'descricao', 'avatar', 'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'id', 'updated_at', 'roles', 'ip'
    ];

    public function isAdmin(): bool
    {
        return $this->roles === 'admin';
    }

}
