<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{

    protected $guarded = [];
    protected $fillable = ['id', 'descricao', 'tags', 'user_id', 'updated_at', 'created_at'];

    public static function getAll(int $id): iterable
    {
        yield self::where('user_id', $id)->get();
    }
}
