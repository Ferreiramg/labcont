<?php

namespace App\Providers;

use Throwable;
use Brazanation\Documents\Cnpj;
use function \preg_match, \trim;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {  
      
    }

    public function boot()
    {
        Validator::extend('cnpj', function ($attribute, $value, $parameters, $validator) {
            try {
                $cnpj = new Cnpj($value);
                return true;
            } catch (Throwable $th) {
                return false;
            }
        });
    
    }
}
