<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Demonstrativos extends Model
{
    protected  $guarded = [];
    protected $primaryKey = 'conta_id';
}
