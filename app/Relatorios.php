<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use DateTime;

class Relatorios extends Model
{
     public function balanceteContas(string $start, string $end, int $eid)
     {

          $sql = "SELECT pcx.nome,pcx.classificacao,pcx.natureza 
                         , abs(sum(xx.saldo_inicial)) AS saldo_inicial 
                         , sum(xx.entrada)       AS entrada
                         , sum(xx.saida)         AS saida  
                         , abs (
                         case when natureza = 'D' then 
                                        sum(xx.saldo_inicial) + sum(xx.entrada)  - sum(xx.saida) 
                              when natureza = 'C' then 
                                        sum(xx.saldo_inicial) - sum(xx.entrada)  + sum(xx.saida) 
                              else 
                                   0.00 
                         end ) as saldo_final
                         --, (case when sum(xx.saldo_inicial) < 0 then 'Devedora' else 'Credora' end) AS natureza_sa
                    FROM
                    (
                    SELECT x.conta_id,
                         coalesce(
                         (
                         SELECT sum
                                   ( 
                                   CASE WHEN conta_tipo = 'D' THEN 
                                             valor
                                        WHEN conta_tipo = 'C' THEN 
                                             valor * -1 
                                        ELSE 
                                             0.00
                                   END
                                   ) 
                              FROM lancamentos 
                                   WHERE movimento < '{$start}'
                                   AND empresa_id = {$eid}
                                   AND conta_id  = x.conta_id 
                         ),0)  AS saldo_inicial
                         , sum(x.entrada) AS entrada
                         , sum(x.saida)   AS saida 
                    FROM
                         ( 
                         SELECT lan.conta_id
                              , CASE WHEN lan.conta_tipo = 'D' THEN 
                                             lan.valor
                                        ELSE 
                                             0.00 
                                   END AS entrada 
                              , CASE WHEN lan.conta_tipo = 'C' THEN 
                                             lan.valor
                                        ELSE 
                                             0.00 
                                   END AS saida 
                              FROM lancamentos AS lan
                                   WHERE lan.movimento >= '{$start}'
                                   AND lan.movimento <= '{$end}'
                                   AND lan.empresa_id = {$eid}
                         --AND lan.id_plano_conta  = '2.2.2'
                         ) AS x
                         GROUP BY x.conta_id
                    ) AS xx
                         JOIN contas pcx 
                         ON xx.conta_id LIKE pcx.classificacao || '%'
                         AND pcx.empresa_id = {$eid}
                         GROUP BY pcx.nome 
                                   , pcx.classificacao 
                                   , pcx.natureza 
                         ORDER BY pcx.classificacao ASC;";

          yield DB::select($sql);
     }
}
