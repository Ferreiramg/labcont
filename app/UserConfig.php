<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserConfig extends Model
{
    protected $guarded = [];
    protected $fillable = [];
    protected $table = 'users_configs';
    protected $primaryKey = "id";
}
