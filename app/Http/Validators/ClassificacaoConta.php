<?php

namespace App\Http\Validators;

use App\Contas;
use Illuminate\Contracts\Validation\Rule as ValidationRule;

class ClassificacaoConta implements ValidationRule
{
    private $id;
    private $value;
    public function __construct(int $id)
    {
        $this->id = $id;
    }
    public function passes($attribute, $value)
    {
        $this->value = $value;
        return strlen($value) > 0 && Contas::uniqueByUser($this->id, $value);
    }
    public function message()
    {
        return "Conta [{$this->value}] não pode ser duplicada!";
    }
}
