<?php

namespace App\Http\Controllers;

use App\Contas;
use App\Demonstrativos;
use App\User;
use App\Http\Validators\ClassificacaoConta;
use App\UserConfig;
use Illuminate\Http\{Request, Response};
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class Conta extends Controller
{

    private $userId = 0;
    public function __construct(Request $request)
    {
        $this->userId = $request->userId;
    }
    public function analiticas()
    {
        return response()->json(iterator_to_array(Contas::analiticas($this->userId))[0], Response::HTTP_OK);
    }
    public function index()
    {
        return response()->json(iterator_to_array(Contas::getAllByUser($this->userId))[0], Response::HTTP_OK);
    }

    public function nextId()
    {
        $redutor = Contas::lastRecordId($this->userId);
        $redutor = $redutor !==null ? $redutor + 1 : 1;

        return response()->json(['next_id' => $redutor], Response::HTTP_OK);
    }

    public function update(Request $request)
    {
        //   DB::beginTransaction();
        $this->validate($request, [
            'cclassificacao' => 'required',
            'cdescricao' => 'required'
        ], $this->input_error);

        try {
            $id = intval($request->input('id'));
            $classificacao = $request->input('cclassificacao');
            $redutor = Contas::findOrFail($id);

            $conta =  DB::table('contas')
                ->where('id',  $id)
                ->update([
                    'nome' => $request->input('cdescricao'),
                    'classificacao' => $classificacao,
                    'redutor' => $redutor->redutor,
                    'tipo' => $request->input('ctipo'),
                    'natureza' => (string)$request->input('natureza'),
                    'cstatus' => $request->input('cativo'),
                    'parent_id' => $this->parentIdGenerator($classificacao),
                    // 'updated_at' => date('Y-m-d H:i:s')
                ]);

            if ($conta > 0 && $request->has('cgrupo')) {
                // Demonstrativos::destroy(array_map(fn (array $v) => $v['value'], $request->input('cgrupo')));
                Demonstrativos::where('conta_id', $id)->delete();
                foreach ($request->input('cgrupo') as $did) {
                    Contas::insertDemonstrativos(intval($did['value']), $id);
                }
            }
            //  DB::commit();
        } catch (\Exception | QueryException $exception) {
            // DB::rollBack();
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return response()->json([$conta], Response::HTTP_OK);
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'cclassificacao' => ['required', new ClassificacaoConta($request->userId)],
            'cdescricao' => 'required'
        ], $this->input_error);

        try {
            $empSelect= UserConfig::where('user_id',$this->userId)->first();
            $redutor = Contas::lastRecordId($this->userId);
            $redutor = $redutor !==null ? $redutor + 1 : 1;

            $conta =  Contas::create([
                'nome' => $request->input('cdescricao'),
                'classificacao' => $c = $request->input('cclassificacao'),
                'redutor' => $redutor,
                'tipo' => $request->input('ctipo'),
                'natureza' => (string)$request->input('natureza'),
                'cstatus' => $request->input('cativo'),
                'parent_id' => $this->parentIdGenerator($c),
                'user_id' => $request->userId,
                'empresa_id'=> $empSelect->empresa_id
            ]);

            if ($request->has('cgrupo'))
                foreach ($request->input('cgrupo') as $did) {
                    Contas::insertDemonstrativos(intval($did['value']), $conta->id);
                }
        } catch (\Exception | QueryException $exception) {

            return response()->json(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return response()->json([$conta->id + 1], Response::HTTP_OK);
    }

    public function delete(Request $request)
    {
        try {
            $contas = Contas::findOrFail($request->input('id'));
            $contas->delete();
            return response()->json(['id' => $contas->id], Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Conta vínculada a Lançamentos, não pode ser apagado!'], Response::HTTP_BAD_REQUEST);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    private function parentIdGenerator($input): ?int
    {
        if (false === strpos($input, '.')) {
            return null;
        }
        $array = explode('.', $input);
        array_pop($array);
        $input = implode('.', $array);
        $parent = Contas::where('user_id', $this->userId)->where('classificacao',  $input)->first();
        //tenho q verificar se a analitica ou sintetica
        if (!$parent) {
            throw new \InvalidArgumentException("Conta classificação {$input} não existe!");
        }
        return $parent->id;
    }

    private function array_sort($array, $on, $order = SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
}
