<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;



class Controller extends BaseController
{
    protected array $input_error = [
        'required' => 'O :attribute e obrigatório!',
        'cnpj' => ':value não é um CNPJ valido!',
        'cep' => ':value não é um CEP valido!',
        'max' => 'O :attribute é muito grande',
        'min' => 'O :attribute deve ser maior que :min',
        'in' => 'The :attribute must be one of the following types: :values',
    ];
}
