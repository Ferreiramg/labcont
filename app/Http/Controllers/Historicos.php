<?php

namespace App\Http\Controllers;

use App\Historico;
use Exception;
use Illuminate\Http\{Request, Response};
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Throwable;

class Historicos extends Controller
{

    public function index(Request $request)
    {
        return response()->json(iterator_to_array(Historico::getAll($request->userId))[0], Response::HTTP_OK);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'descricao' => 'required'
        ], $this->input_error);
        
        try {
            $tags = $request->has('tags') ? join(', ', $request->input('tags')) : null;
            DB::table('historicos')
                ->where('id', intval($request->input('id')))
                ->update([
                    'descricao' => $request->input('descricao'),
                    'tags' => $tags,
                    'updated_at' => date('Y-m-d H:i:s', date('now'))
                ]);
            return  response()->json([$request->all()], Response::HTTP_OK);
        } catch (Exception | QueryException $exception) {
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    public function store(Request $request)
    {

        $this->validate($request, [
            'descricao' => 'required'
        ], $this->input_error);

        try {
            $tags = $request->has('tags') ? join(', ', $request->input('tags')) : null;
            $create = Historico::create([
                'descricao' => $request->input('descricao'),
                'tags' => $tags,
                'user_id' => $request->userId
            ]);
            return  response()->json([$create], Response::HTTP_OK);
        } catch (\Throwable | QueryException $exception) {
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete(Request $request)
    {
        try {
            $lancamento = Historico::findOrFail($request->input('id'));
            $lancamento->delete();
            return response()->json(['id' => $lancamento->id], Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Histórico vínculado à Lançamentos!'], Response::HTTP_BAD_REQUEST);
        } catch (Throwable $th) {
            return response()->json(['message' => $th->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
