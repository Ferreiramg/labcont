<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Operacoes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Empresas extends Controller
{

    public function update(Request $request)
    {
        $this->validate($request, [
            "id" => "required",
            "cnpj" => ["required", "cnpj", \Illuminate\Validation\Rule::unique('empresas')->ignore($request->input('id'))],
            "email" => "email",
            "nome" => "required|min:5",
            "inicio" => "required",
            "fantasia" => "required",
            "cidade" => "required",
            "uf" => "required",
            "rua" => "required",
            "numero" => "required",
            "cep" => "required|regex:/^[0-9]{5}-[0-9]{3}/",
            "n_juridica" => "required",
            "insc_estatual" => ["required", \Illuminate\Validation\Rule::unique('empresas')->ignore($request->input('id'))]
        ], $this->input_error);

        $empresa = Empresa::findOrFail($request->input('id'));
        $input = $request->all();
        $save =  $empresa->update([
            'nome' => $input['nome'],
            'cnpj' => $input['cnpj'],
            'email' => $input['email'],
            'fantasia' => $input['fantasia'],
            'status' => $input['ativada'],
            'cidade' => $input['cidade'],
            'uf' => $input['uf'],
            'cep' => $input['cep'],
            'rua' => $input['rua'],
            'bairro' => $input['bairro'] ?? null,
            'numero' => $input['numero'],
            'code_cidade' => $input['ibge'],
            'inicio' =>  date("Y-m-d H:i:s", strtotime($input['inicio'])),
            'suframa' => $input['suframa'] ?? null,
            'natureza_juridica' => $input['n_juridica'],
            'insc_estatual' => $input['insc_estatual'],
            'insc_municipal' => $input['im'] ?? null,
        ]);
        return response()->json([$save], Response::HTTP_OK);
    }
    public function store(Request $request)
    {

        $this->validate($request, [
            "cnpj" => "required|cnpj|unique:empresas",
            "email" => "email",
            "nome" => "required|min:5",
            "inicio" => "required",
            "fantasia" => "required",
            "cidade" => "required",
            "uf" => "required",
            "rua" => "required",
            "numero" => "required",
            "cep" => "required|regex:/^[0-9]{5}-[0-9]{3}/",
            "n_juridica" => "required",
            "insc_estatual" => "required|unique:empresas"
        ], $this->input_error);

        $input = $request->all();
        Empresa::create([
            'nome' => $input['nome'],
            'cnpj' => $input['cnpj'],
            'email' => $input['email'],
            'fantasia' => $input['fantasia'],
            'status' => $input['ativada'],
            'cidade' => $input['cidade'],
            'uf' => $input['uf'],
            'cep' => $input['cep'],
            'rua' => $input['rua'],
            'bairro' => $input['bairro'] ?? null,
            'numero' => $input['numero'],
            'code_cidade' => $input['ibge'],
            'inicio' =>  date("Y-m-d H:i:s", strtotime($input['inicio'])),
            'suframa' => $input['suframa'] ?? null,
            'natureza_juridica' => $input['n_juridica'],
            'insc_estatual' => $input['insc_estatual'],
            'insc_municipal' => $input['im'] ?? null,
            'user_id' => $request->userId
        ]);
        return response()->json('success', Response::HTTP_OK);
    }

    public function insertOperacao(Request $request)
    {
        $descricao = (string) $request->input('descricao') ?? null;
        if (Operacoes::inserirOperacao((string) $request->input('tipo'), (string) $request->input('nome'), $descricao, 'now'))
            return response()->json('success', Response::HTTP_OK);

        return response()->json('error insert', Response::HTTP_BAD_REQUEST);
    }

    public function listOperacoes()
    {
        return response()->json(iterator_to_array(Operacoes::listarOperacoes())[0], Response::HTTP_OK);
    }

    public function listByUser(Request $request)
    {
        return response()->json(iterator_to_array(Empresa::getAllByUser($request->userId))[0], Response::HTTP_OK);
    }
}
