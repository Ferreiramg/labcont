<?php

namespace App\Http\Controllers;

use App\UserConfig;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class User extends Controller
{

    public function index(Request $request)
    {
        return response()->json($request->userAuth, Response::HTTP_OK);
    }

    public function getConfig(Request $request)
    {
       $config = UserConfig::where('user_id',$request->userId)->first();
        return  response()->json([$config], Response::HTTP_OK);
    }
    public function setConfig(Request $request)
    {
        $user = UserConfig::firstOrNew(['user_id' => $request->userId]);
        $user->empresa_id = intval($request->input('empresa_id'));
        $user->user_id = $request->userId;

        $user->save();
        return  response()->json([$user], Response::HTTP_OK);
    }

    public function upload(Request $request)
    {
        try {
            $this->validate($request, [
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $image = $request->file('file');
            $name = "avatar-{$request->userId}.{$image->getClientOriginalExtension()}";
            $destinationPath = public_path('/static/media');
            $image->move($destinationPath, $name);
            $request->userAuth->fill(['avatar' => $name])->save();

            return  response()->json(['avatar' => $name], Response::HTTP_OK);
        } catch (FileException | QueryException $th) {
            return response()->json(['message' => $th->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        try {
            $save =  $request->userAuth->fill(
                [
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'endereco' => $request->input('endereco'),
                    'descricao' => $request->input('descricao'),
                    'cidade' => $request->input('cidade'),
                    'cep' => $request->input('cep'),
                    'ip' => $request->ip(),
                    'updated_at' => date('Y-m-d H:i:s', strtotime('now'))
                ]
            )->save();

            return  response()->json([$save], Response::HTTP_OK);
        } catch (\Throwable | QueryException $exception) {
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
