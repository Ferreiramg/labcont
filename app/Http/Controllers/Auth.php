<?php

namespace App\Http\Controllers;

use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Firebase\JWT\ExpiredException;

use function password_verify, password_hash;

class Auth extends Controller
{

    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    protected function jwt(User $user)
    {
        $payload = [
            'iss' => "labcontabil-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60 * 60 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return ['token' => JWT::encode($payload, env('JWT_SECRET')), 'expires_in' => $payload['exp']];
    }
    public function refresh()
    {
        $token = $this->request->get('token');
        try {
            JWT::$leeway = 720000;
            $decoded = (array) JWT::decode($token, env('JWT_SECRET'), ['HS256']);
            $decoded['iat'] = time();
            $decoded['exp'] = time() + 60 * 60 * 30;

            return response()->json([
                'token' => JWT::encode($decoded, env('JWT_SECRET')),
                'expires_in' => $decoded['exp']
            ], Response::HTTP_OK);
        } catch (ExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired.'
            ], Response::HTTP_UNAUTHORIZED);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'An error while decoding token.'
            ], Response::HTTP_UNAUTHORIZED);
        }
    }
    public function authenticate(User $user)
    {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);
        // Find the user by email
        $user = User::where('email', $this->request->input('email'))->first();

        if (!$user) {
            return response()->json([
                'email' => 'Email não existe.'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        // Verify the password and generate the token
        if (password_verify($this->request->input('password'), $user->password)) {
            return response()->json($this->jwt($user), Response::HTTP_OK);
        }
        // Bad Request response
        return response()->json([
            'password' => 'Senha inválida.'
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function register()
    {
        $this->validate($this->request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password'  => 'required'
        ]);

        $user = User::create([
            'name' => $this->request->input('name'),
            'email' => $this->request->input('email'),
            'password' => password_hash($this->request->input('password'), PASSWORD_DEFAULT),
        ]);

        return response()->json($this->jwt(User::where('email', $this->request->input('email'))->first()), Response::HTTP_OK);
    }
}
