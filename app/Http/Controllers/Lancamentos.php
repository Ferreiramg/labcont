<?php

namespace App\Http\Controllers;

use App\Lancamento;
use App\UserConfig;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class Lancamentos extends Controller
{

    public function delete(Request $request)
    {
        $lancamento = Lancamento::findOrFail($request->input('id')); //delete by uuid
        $lancamento->delete();
        return response()->json(['id' => $lancamento->id], Response::HTTP_OK);
    }
    public function diaryReport(Request $request)
    {
        $result = [];
        foreach (Lancamento::getAllFromUser($request->userId) as $key => $value) {

            $result[$value->movimento][] = (array) $value;
        }
        return response()->json($result, Response::HTTP_OK);
    }

    public function listByUser(Request $request)
    {
        $result = [];
        $join = false;
        foreach (Lancamento::getAllFromUser($request->userId) as $key => $value) {
            $result[$value->id] = (array) $value;
        }
        return response()->json($result, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "lote" => "required",
            "total" => "required",
            "rtt" => "required:boolean",
            "ldata" => "required",
            "c" => "required",
            "d" => "required",
            "historico" => "required",
            "empresa" => "required"
        ], $this->input_error);

        $input = $request->all();

        $insert = DB::table('lancamentos')->insert($this->resolveContas($input, $request));

        return response()->json($insert, Response::HTTP_OK);
    }

    private function resolveContas($input, $request)
    {
        $debito = $input['d'];
        $credito = $input['c'];
        $toInsert = [];
        $total = 0;
        $uuid =  uniqid('', true);
        foreach ($debito as $key => $value) {
            $total += floatVal($value['valor']['value']);
            $toInsert[] =  [
                'lote' => $input['lote'],
                'uuid' => $uuid,
                'movimento' => date("Y-m-d H:i:s", strtotime($input['ldata'])),
                'valor' =>   floatVal($value['valor']['value']),
                'total' =>  $key === 0 ?  $input['total'] : 0,
                'rtt' => $input['rtt'],
                'conta_id' => \App\Contas::where('id',  (int) $value['cdebito']['value'])->first()->classificacao,
                'conta_tipo' => 'D',
                'historico_id' => $input['historico'],
                'user_id' => $request->userId,
                'empresa_id' => $input['empresa'],
                'created_at' => date('Y-m-d H:i:s')
            ];
        }
        foreach ($credito as $key => $value) {
            $toInsert[] =  [
                'lote' => $input['lote'],
                'uuid' => $uuid,
                'movimento' => date("Y-m-d H:i:s", strtotime($input['ldata'])),
                'valor' => floatVal($value['valor']['value']),
                'total' => 0,
                'rtt' => $input['rtt'],
                'conta_id' => \App\Contas::where('id',  (int) $value['ccredito']['value'])->first()->classificacao,
                'conta_tipo' => 'C',
                'historico_id' => $input['historico'],
                'user_id' => $request->userId,
                'empresa_id' => $input['empresa'],
                'created_at' => date('Y-m-d H:i:s')
            ];
        }

        return $toInsert;
    }

    public function somaSinteticas(Request $request)
    {
        $empSelect = UserConfig::where('user_id', $request->userId)->first();
        $data =  Lancamento::sumContasSinteticas($request->userId, $empSelect->empresa_id);

        return response()->json(iterator_to_array($data)[0], Response::HTTP_OK);
    }
}
