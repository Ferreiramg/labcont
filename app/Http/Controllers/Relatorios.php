<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Relatorios as AppRelatorios;
use App\UserConfig;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Relatorios extends Controller
{
    public function balancete($start, $end, $id)
    {
        $model = new AppRelatorios();
        
        return response()->json(
            iterator_to_array(
                $model->balanceteContas(
                    (string) $start,
                    (string) $end,
                    (int) $id
                )
            )[0],
            Response::HTTP_OK
        );
    }

    public function downloadExcell(Request $request)
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();

        $table = $request->input('payload');
        $spreadsheet = $reader->loadFromString($table);
        $factor = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');

        $doc = new \DOMDocument();
        $doc->loadHTML($table);
        $xpath = new \DOMXpath($doc);
        $name = $xpath->query("//table[contains(@class,'text-center')]");

        $file = "relatorio.xls";
        $factor->save($file);

        return  response()->json(['download' => $name->item(0)],  Response::HTTP_OK);
    }
}
