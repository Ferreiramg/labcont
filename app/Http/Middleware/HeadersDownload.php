<?php

namespace App\Http\Middleware;

use Closure;

class HeadersDownload
{

    public function handle($request, Closure $next)
    {
        $headers = [
            'Content-type'      => 'application/vnd.ms-excel',
            'Content-Disposition'     => 'attachment; filename="relatorio.xlsx"',
        ];

        $response = $next($request);
        foreach ($headers as $key => $value) {
            $response->header($key, $value);
        }

        return $response;
    }
}
