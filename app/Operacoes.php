<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Operacoes extends Model
{

    public static function listarOperacoes(): iterable
    {
        yield DB::select('select * from empresa_parametros');
    }
    public static function inserirOperacao(...$parametros): bool
    {
        return DB::insert('insert into empresa_parametros (tipo, nome, descricao, created_at) values (?, ?, ?, ?)', $parametros);
    }
}
