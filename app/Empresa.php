<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Empresa extends Model
{

    protected  $guarded = [];
    protected $fillable = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getAllByUser(int $id): iterable{
        yield self::where('user_id',$id)->get();
    }
}
