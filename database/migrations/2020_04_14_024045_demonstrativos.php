<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Demonstrativos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demonstrativos', function (Blueprint $table) {
            $table->integer('demonstrativo_id')->unsigned();
            
            $table->foreign('demonstrativo_id')
                ->references('id')
                ->on('empresa_parametros')
                ->constrained()
                ->onDelete('cascade');
            $table->foreignId('conta_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demonstrativos');
    }
}
