<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Lancamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lancamentos', function (Blueprint $table) {

            $table->increments('id');
            $table->string('lote');
            $table->string('uuid')->nullable();
            $table->date('movimento');
            $table->decimal('valor', 10);
            $table->decimal('total', 10)->default(0);
            $table->boolean('rtt')->default(false);
            $table->text('observacao')->nullable();
            $table->string('conta_tipo');
            $table->string('conta_id');
            $table->integer('historico_id')->unsigned()->nullable();

            // $table->foreign('conta_id')->references('id')->on('contas');
            $table->foreign('historico_id')->references('id')->on('historicos');

            $table->foreignId('empresa_id')->constrained()->onDelete('cascade'); //um pra um
            $table->foreignId('user_id')->constrained()->onDelete('cascade'); //um pra um

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lancamentos');
    }
}
