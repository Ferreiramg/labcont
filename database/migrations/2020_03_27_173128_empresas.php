<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Empresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email')->nullable();
            $table->string('fantasia')->nullable();
            $table->string('cnpj')->unique();
            $table->date('inicio');
            $table->boolean('status');
            $table->string('rua');
            $table->string('numero');
            $table->string('bairro');
            $table->string('cidade');
            $table->string('uf');
            $table->string('cep');
            $table->string('code_cidade')->nullable();
            $table->string('code_uf')->nullable();
            $table->string('natureza_juridica');
            $table->string('insc_estatual')->unique();
            $table->string('insc_municipal')->nullable();
            $table->string('suframa')->nullable();
            $table->foreignId('user_id')->constrained()->onDelete('cascade'); //um pra um

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
