<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Contas as ModelConta;

class ContasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         if (getenv('APP_ENV') === 'production') {
             return null;
         }
        $handler = fopen(__DIR__.'/contas.txt', 'r');
        $c = 1;
        while (($data = fgetcsv($handler, 1000, ";")) !== false) {
            if (!empty($data[0]))
                DB::table('contas')->insert([
                    // 'id' => intval(trim($data[0])),
                    'nome' => trim($data[3]),
                    'classificacao' => trim($data[2]),
                    'redutor' => $c,
                    'cstatus' => true,
                    'user_id' => 1,
                    'parent_id' => parentIdGenerator(trim($data[2])),
                    'tipo' => trim($data[1]) === 'S' ? 2 : 1,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            ++$c;
        }
    }
}
function parentIdGenerator($input): ?int
{
    if (false === strpos($input, '.')) {
        return null;
    }
    $array = explode('.', $input);
    array_pop($array);
    $input = implode('.', $array);
    $parent = ModelConta::where('classificacao',  $input)->first();
    // $parent =   DB::table('contas')->select("select id,classificacao from contas where classificacao='{$input}' limit 1")->get();
    //tenho q verificar se a analitica ou sintetica
    if (!$parent) {
        throw new \InvalidArgumentException("Conta classificação {$input} não existe!");
    }
    return $parent->id;
}
