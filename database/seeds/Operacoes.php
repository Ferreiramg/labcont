<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Operacoes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insert = [
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Balanço Patrimonial (BP)',
                'tipo' => 'demonstrativo',
                'descricao' => 'Balanço Patrimonial é o demonstrativo que apresenta o avanço do patrimônio de uma empresa. Com patrimônio entende-se todos os ativos (bens que geram lucro) e passivos (todas as obrigações financeiras).'
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Demonstração do Resultado do Exercício (DRE)',
                'tipo' => 'demonstrativo',
                'descricao' => 'Ao lado do Balanço Patrimonial e da Demonstração de Fluxo de Caixa (DFC), o DRE é uma das demonstrações financeiras mais importantes. De forma simples, o DRE é o relatório responsável por mostrar (anualmente ou mensalmente) se a empresa está tendo lucro ou prejuízo.'
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Demonstração dos Lucros ou Prejuízos Acumulados (DLPA)',
                'tipo' => 'demonstrativo',
                'descricao' => 'A DLPA é geralmente a última demonstração feita, geralmente ao fim do ano. Isso porque é um acumulado do exercício de um determinado período. O objetivo da DLPA é, além de mostrar os lucros e prejuízos, tornar mais transparente a quantidade de impostos com base no lucro pagos pela empresa'
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Demonstração de Fluxo de Caixa (DFC)',
                'tipo' => 'demonstrativo',
                'descricao' => 'Essa provavelmente é a demonstração mais comum de toda empresa, e deve ser regra básica para o controle financeiro. O DFC é todo o controle de entrada e saída monetária da sua empresa.'
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Demonstração de Valor Adicionado (DVA)',
                'tipo' => 'demonstrativo',
                'descricao' => 'A Demonstração de Valor Adicionado (DVA) é o relatório que apresenta os valores monetários conquistados pela empresa e como foram distribuídos durante o exercício.'
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Microempreendedor Individual (MEI)',
                'tipo' => 'natureza',
                'descricao' => 'A forma mais simples e rápida de empreender é com o MEI, ou o Microempreendedor Individual, pois ele permite obter um CNPJ rapidamente e sem muita burocracia, emitir notas fiscais pelos produtos ou serviços prestados e ainda pagar os impostos de uma só vez em uma única guia.'
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Empresário Individual (EI)',
                'tipo' => 'natureza',
                'descricao' => 'Sigla para Empresário Individual ou Empresa Individual, o EI é semelhante ao MEI no sentido de ser uma empresa de apenas um único dono e sem sócios. '
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Empresa Individual de Responsabilidade Limitada (EIRELI)',
                'tipo' => 'natureza',
                'descricao' => 'Não é necessário a participação de sócios, assim como um Empresário Individual. O dono desse tipo de empresa pode tomar todas as decisões sozinho e será total e unicamente responsável por ela. '
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Sociedade Anônima (S/A)',
                'tipo' => 'natureza',
                'descricao' => 'Outra forma de sociedade é a Sociedade Anônima, ou S/A, não possui sócios, mas, sim, acionistas, pois, ao invés de cotas, divide-se o capital em ações. Graças a isso, esses acionistas podem comprar e vender suas ações livremente – não é à toa que é o tipo de natureza jurídica geralmente escolhido por grandes corporações.'
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Sociedade Simples Limitada (Ltda)',
                'tipo' => 'natureza',
                'descricao' => 'Esse tipo de natureza jurídica é específico, já que se trata de uma das modalidades da Sociedade Simples, uma empresa prestadora de serviços, formada por dois ou mais sócios do mesmo ramo e normalmente escolhida por profissões intelectuais e de cooperativa como médicos, dentistas, advogados, contadores, etc.'
            ],
            [
                'created_at'=>date('Y-m-d H:i:s'),
                'nome' => 'Sociedade Limitada Unipessoal (SLU)',
                'tipo' => 'natureza',
                'descricao' => 'Finalmente, a SLU, ou Sociedade Limitada Unipessoal, é uma espécie de “mistura” entre os tipos de natureza jurídica de uma empresa, pois, assim como uma EIRELI, não é exigido sócios; como um EI, o investimento para o capital social pode ser baixo; e como uma Ltda., os patrimônios do dono são protegidos, estando separados dos patrimônios da empresa.'
            ],
        ];
        DB::table('empresa_parametros')->insert($insert);
    }
}
