<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Contas as ModelConta;
use App\Historico;

include "./storage/app/CSVIterator.php";
class LancamentosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (getenv('APP_ENV') === 'production') {
            return null;
        }
        $result = [];
        $iterator = new CsvIterator('./storage/app/lance.txt', ';');
        $valor = 0;
        $c = 0;
        foreach ($iterator as $k => $values) {
            if (trim($values[4]) !== "") {
                $valor = 0;
                $c = $k;
                $result[] = [
                    'lote' => $k,
                    'movimento' => date('Y-m-d', strtotime(str_replace('/', '-', trim($values[0])))),
                    'conta_id' => trim($values[1]),
                    'historico_id' => strpos(trim($values[3]), 'DUP') !== false ? 19 : histID(trim($values[3])),
                    'user_id' => 1,
                    'empresa_id' => 1,
                    'conta_tipo' => 'D',
                    'valor' => $valor = number_format(str_replace(",", ".", str_replace(".", "", trim($values[4]))), 2, '.', ''),
                    'created_at' => date('Y-m-d H:i:s')
                ];
            } else {
                $result[] = [
                    'lote' =>  $c,
                    'movimento' => date('Y-m-d', strtotime(str_replace('/', '-', trim($values[0])))),
                    'conta_id' => trim($values[1]),
                    'historico_id' => strpos(trim($values[3]), 'DUP') !== false ? 19 : histID(trim($values[3])),
                    'user_id' => 1,
                    'empresa_id' => 1,
                    'conta_tipo' => 'C',
                    'valor' => $valor,
                    'created_at' => date('Y-m-d H:i:s')
                ];
            }
        }

        DB::table('lancamentos')->insert($result);
    }
}
//php artisan db:seed --class=LancamentosSeed

function histID($input)
{
    $parent = Historico::where('descricao',  $input)->first();
    if (!$parent) {
        throw new \InvalidArgumentException("Conta historico {$input} não existe!");
    }
    return $parent->id;
}
function parentIdGenerator($input)
{
    $parent = ModelConta::where('classificacao',  $input)->first();
    if (!$parent) {
        throw new \InvalidArgumentException("Conta classificação {$input} não existe!");
    }
    return $parent->id;
}
