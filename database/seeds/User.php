<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Luis',
                'email' => 'lp@lp.com.br',
                'password' => password_hash('senha123', PASSWORD_DEFAULT),
                'roles' => 'admin',
                'avatar' => 'logo-small.png',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Rutney',
                'email' => 'rcr@mail.com.br',
                'password' => password_hash('senha123', PASSWORD_DEFAULT),
                'roles' => 'admin',
                'avatar' => 'logo-small.png',
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
