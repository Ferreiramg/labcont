<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Historicos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // if (getenv('APP_ENV') === 'production') {
        //     return null;
        // }
        $r = [];
        foreach (file(__DIR__ . '/hist.txt') as $lines) {
            $r[] = ['descricao' => trim($lines), 'user_id' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }
        DB::table('historicos')->insert($r);
    }
}
