 SELECT pcx.nome,pcx.classificacao
       , sum(xx.saldo_inicial) AS saldo_inicial 
       , sum(xx.entrada)       AS entrada
       , sum(xx.saida)         AS saida 
       , sum(xx.saldo_final)   AS saldo_final 
    FROM
       (
        SELECT x.conta_id
             , coalesce(
               (
                SELECT sum
                       ( 
                        CASE WHEN conta_tipo = 'D' THEN 
                                  valor
                             WHEN conta_tipo = 'C' THEN 
                                  valor * -1 
                             ELSE 
                                  0.00
                        END
                       ) 
                  FROM lancamentos 
                 WHERE movimento < '2018-01-04'
                   AND conta_id  = x.conta_id 
               ),0)  AS saldo_inicial 
             , sum(x.entrada) AS entrada
             , sum(x.saida)   AS saida
             , coalesce(
               (
                 SELECT sum
                        ( 
                         CASE WHEN conta_tipo = 'D' THEN 
                                  valor
                             WHEN conta_tipo = 'C' THEN 
                                  valor * -1 
                             ELSE 
                                  0.00
                         END
                        ) 
                   FROM lancamentos
                  WHERE movimento <=  '2018-01-04'
                    AND conta_id  = x.conta_id 
               ),0)  AS saldo_final
          FROM
             ( 
               SELECT lan.conta_id
                    , CASE WHEN lan.conta_tipo = 'C' THEN 
                                lan.valor
                           ELSE 
                                0.00 
                      END AS entrada 
                    , CASE WHEN lan.conta_tipo = 'D' THEN 
                                lan.valor
                           ELSE 
                                0.00 
                      END AS saida 
                 FROM lancamentos AS lan
                WHERE lan.movimento >= '2018-01-26'
                  AND lan.movimento <= '2018-03-26'
                --AND lan.id_plano_conta  = '2.2.2'
             ) AS x
      GROUP BY x.conta_id
       ) AS xx
    JOIN contas pcx 
      ON xx.conta_id LIKE pcx.classificacao || '%'
GROUP BY pcx.nome 
       , pcx.classificacao 
ORDER BY pcx.classificacao ASC
       ; 

       //funcional
          $sql = "
         SELECT pcx.nome,pcx.classificacao, xx.tipo
       , abs(xx.saldo_inicial) AS saldo_inicial 
       , sum(xx.entrada)       AS entrada
       , sum(xx.saida)         AS saida 
       , abs(xx.saldo_final)   AS saldo_final 
       , (saldo_inicial - entrada + saida) AS saldo_atual
       , (case when saldo_inicial < 0 then 'D' else 'C' end) AS natureza_sa,
    FROM
       (
        SELECT 
               x.conta_id
             , (case when x.entrada > x.saida then 'D' else 'C' end) AS natureza
             , coalesce(
               (
                SELECT sum
                       ( 
                        CASE WHEN conta_tipo = 'C' THEN 
                                  valor
                             WHEN conta_tipo = 'D' THEN 
                                  valor * -1 
                             ELSE 
                                  0.00
                        END
                       ) 
                  FROM lancamentos 
                 WHERE movimento < '{$start}'
                   AND empresa_id = {$eid}
                   AND conta_id  = x.conta_id 
               ),0)  
               AS saldo_inicial 
             , sum(x.entrada) AS entrada
             , sum(x.saida)   AS saida
             , coalesce(
               (
                 SELECT sum
                        ( 
                         CASE WHEN conta_tipo = 'D' THEN 
                                  valor 
                             WHEN conta_tipo = 'C' THEN 
                                  valor * -1 
                             ELSE 
                                  0.00
                         END
                        )  
                   FROM lancamentos
                  WHERE movimento <= '{$end}'
                    AND empresa_id = {$eid}
                    AND conta_id  = x.conta_id 
               ),0)  AS saldo_final
          FROM
             ( 
               SELECT lan.conta_id
                    , CASE WHEN lan.conta_tipo = 'C' THEN 
                                lan.valor
                           ELSE 
                                0.00 
                      END AS entrada 
                    , CASE WHEN lan.conta_tipo = 'D' THEN 
                                lan.valor
                           ELSE 
                                0.00 
                      END AS saida 
                 FROM lancamentos AS lan
                WHERE lan.movimento >= '{$start}'
                  AND lan.movimento <= '{$end}'
                  AND empresa_id = {$eid}
                --AND lan.id_plano_conta  = '2.2.2'
             ) AS x
      GROUP BY x.conta_id
       ) AS xx
    JOIN contas pcx ON xx.conta_id LIKE pcx.classificacao || '%'
GROUP BY pcx.nome, pcx.classificacao 
ORDER BY pcx.classificacao ASC; 
";